"use strict";

module.exports = function() {
	const api = this;
	const { service } = api;

	/**
	 * Implements policy of plugin-cookies.
	 */
	class CookiesPolicy {
		/**
		 * Extracts cookies from request header and exposes them as separate map of
		 * values.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next to be invoked when policy is done processing request
		 * @returns {void}
		 */
		static parse( req, res, next ) {
			req.cookies = service.Cookies.parseFromString( req.headers.cookie, true ); // eslint-disable-line no-param-reassign

			next();
		}
	}

	return CookiesPolicy;
};

