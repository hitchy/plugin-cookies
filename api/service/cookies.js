"use strict";

module.exports = function() {
	const api = this;

	const logDebug = api.log( "hitchy:cookies:debug" );

	/**
	 * Implements service providing methods for parsing cookies from a request
	 * or a string.
	 */
	class CookiesService {
		/**
		 * Extracts cookies from provided request's header and returns them as
		 * set of key-value pairs.
		 *
		 * @param {Hitchy.Core.IncomingMessage} request request to extract cookies from its header
		 * @returns {Object<string,string>} map of cookies' names into either one's value
		 */
		static parseFromRequest( request ) {
			return this.parseFromString( request.headers.cookie );
		}

		/**
		 * Extracts cookies from provided string assumed to comply with format
		 * of the HTTP request header named `Cookie`.
		 *
		 * @param {string} input serialized version of cookies e.g. found in an HTTP request `Cookie` header
		 * @param {boolean} debug if true, found cookies and their values are logged for debugging
		 * @returns {Object<string,string>} map of cookies' names into either one's value
		 */
		static parseFromString( input, debug = false ) {
			const parsed = {};
			const pattern = /([^=]+)=([^;]*)(?:;|$)/g;

			if ( typeof input === "string" ) {
				let match;

				while ( ( match = pattern.exec( input ) ) ) {
					const name = match[1].trim();
					const value = match[2];

					if ( debug ) {
						logDebug( `found cookie ${name}="${value}"` );
					}

					if ( parsed.hasOwnProperty( name ) ) {
						if ( Array.isArray( parsed[name] ) ) {
							parsed[name].push( value );
						} else {
							parsed[name] = [ parsed[name], value ];
						}
					} else {
						parsed[name] = value;
					}
				}
			}

			return parsed;
		}
	}

	return CookiesService;
};
