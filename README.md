# @hitchy/plugin-cookies [![pipeline status](https://gitlab.com/hitchy/plugin-cookies/badges/master/pipeline.svg)](https://gitlab.com/hitchy/plugin-cookies/-/commits/master)

_per-request cookie parser for [Hitchy](https://hitchy.org/)_

## License

[MIT](LICENSE)

## Usage

In your [Hitchy-based](https://hitchy.org/) application run
 
```
npm install --save @hitchy/plugin-cookies
```

On start of your application, it is discovering the plugin which is automatically injecting a new policy parsing every request's header for provided cookies exposing them in [`req.cookies`](https://core.hitchy.org/api/#req-cookies) in handlers of any succeeding policy or controller.
