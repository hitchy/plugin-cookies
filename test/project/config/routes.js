"use strict";

exports.routes = {
	"/": function( req, res ) {
		res.json( req.cookies || {} );
	},
	"/some/subordinated/url": function( req, res ) {
		res.json( req.cookies || {} );
	},
	"POST /some/subordinated/url/posted": function( req, res ) {
		res.json( req.cookies || {} );
	},
};
