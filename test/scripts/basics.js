"use strict";

const { describe, before, after, it } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );

require( "should" );
require( "should-http" );

describe( "Hitchy w/ plugin for parsing cookies", () => {
	const ctx = {};

	after( SDT.after( ctx ) );
	before( SDT.before( ctx, {
		plugin: true,
		projectFolder: "../project",
	} ) );

	it( "is running", async() => {
		const res = await ctx.get( "/" );

		res.should.have.status( 200 );
	} );

	it( "is returning detected set of provided cookies", async() => {
		const res = await ctx.get( "/", {
			cookie: "scalar=1; list=first; list=second; counter=5",
		} );

		res.should.have.status( 200 );
		res.data.should.be.Object().which.has.property( "scalar" ).which.is.equal( "1" );
		res.data.should.be.Object().which.has.property( "list" ).which.is.an.Array().and.deepEqual( [ "first", "second" ] );
		res.data.should.be.Object().which.has.property( "counter" ).which.is.equal( "5" );
	} );

	it( "is returning detected set of provided cookies using URL different from parsing policy", async() => {
		const res = await ctx.get( "/some/subordinated/url", {
			cookie: "scalar=3; list=a; list=2; counter=4",
		} );

		res.should.have.status( 200 );
		res.data.should.be.Object().which.has.property( "scalar" ).which.is.equal( "3" );
		res.data.should.be.Object().which.has.property( "list" ).which.is.an.Array().and.deepEqual( [ "a", "2" ] );
		res.data.should.be.Object().which.has.property( "counter" ).which.is.equal( "4" );
	} );

	it( "is returning detected set of provided cookies using URL different from parsing policy and different method", async() => {
		const res = await ctx.post( "/some/subordinated/url", null, {
			cookie: "scalar=12; list=one; list=zwei; counter=1",
		} );

		res.should.have.status( 200 );
		res.data.should.be.Object().which.has.property( "scalar" ).which.is.equal( "12" );
		res.data.should.be.Object().which.has.property( "list" ).which.is.an.Array().and.deepEqual( [ "one", "zwei" ] );
		res.data.should.be.Object().which.has.property( "counter" ).which.is.equal( "1" );
	} );
} );
