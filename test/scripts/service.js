"use strict";

const { describe, before, after, it } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );

require( "should" );

describe( "Cookies service", () => {
	const ctx = {};

	after( SDT.after( ctx ) );
	before( SDT.before( ctx, {
		plugin: true,
	} ) );

	it( "is available", () => {
		ctx.hitchy.api.service.Cookies.should.be.ok();
	} );

	it( "has method parseFromRequest() parsing cookies from a given HTTP request", () => {
		ctx.hitchy.api.service.Cookies.parseFromRequest( {
			headers: {
				cookie: "scalar=1; list=first; list=second; counter=5",
			}
		} ).should.deepEqual( {
			scalar: "1",
			list: [ "first", "second" ],
			counter: "5",
		} );
	} );

	it( "has method parseFromString() parsing cookies from a given string", () => {
		ctx.hitchy.api.service.Cookies.parseFromString( "scalar=3; list=a; list=2; counter=4" ).should.deepEqual( {
			scalar: "3",
			list: [ "a", "2" ],
			counter: "4",
		} );
	} );
} );
